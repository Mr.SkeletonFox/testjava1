import java.util.Random;


public class questfromAlena {

    public static void main(String[] args) {
        Random randomise = new Random();
        int x, y;
        for (; ; ) {
            do {
                x = randomise.nextInt(1_501);   // MAX number of rows = 1250.
                y = (randomise.nextInt(104) + 2);// MAX number of columns  = 105.
            } while ((x * y) >= 40_000_000);
            //checking for minimum compliance with the conditions,
            // otherwise everything will start from the beginning.
            if (x <= 1000 & y <= 100) {
                int[][] mass = new int[y][x];
                System.out.println("FIRST WAY");
                for (int y_1 = 0; y_1 < y; y_1++) {
                    for (int x_1 = 0; x_1 < x; x_1++) {
                        mass[y_1][x_1] = randomise.nextInt(100);     //padding with random values.
                        if (y_1 == 2 & x_1 == 1) System.out.print("  ");
                            // Don't output the 3rd element in the 2 row of table.
                        else System.out.print(mass[y_1][x_1] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println("SECOND WAY");
                System.out.println();

                for (int y_1 = 0; y_1 < y; y_1++) {
                    for (int x_1 = 0; x_1 < x; x_1++) {
                        mass[y_1][x_1] = randomise.nextInt(10);     //padding with random values.


                        if (y_1 == 2 & x_1 == 1)
                             continue; // Don't output the 3rd element in the 2 row of table.
                        System.out.println( "[" + (y_1 + 1) + "]" + "[" +  (x_1 + 1) + "]" + mass[y_1][x_1]);


                    }

                }
                System.out.println();
                System.out.println("THIRD WAY");
                System.out.println();
                for (int y_1 = 0; y_1 < y; y_1++) {
                    for (int x_1 = 0; x_1 < x; x_1++) {
                        mass[y_1][x_1] = randomise.nextInt(10);     //padding with random values.
                        situation:
                        {
                            if (y_1 == 2 & x_1 == 1)
                                break situation; // Don't output the 3rd element in the 2 row of table.
                            System.out.println( "[" + (y_1 + 1) + "]" + "[" +  (x_1 + 1) + "]" + mass[y_1][x_1]);
                        }

                    }

                }


                System.exit(0);//ends the program if the array is successfully output
            }
        }
    }
}


