public class PartOneArithmeticOp {
    public static void main(String[] args) {
        int a = 2;
        int b = 5;
        //int result = b * 3 + 20 / 2 * a--;
        int step1 = a--;
        int step2 = b * 3;
        int step3 = 20 / 2;
        int step4 = step3 * step1;
        int step5 = step2 + step4;
        System.out.println("result =" + step5);


    }
}
