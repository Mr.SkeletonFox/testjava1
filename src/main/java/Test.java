public class Test {
    public static void main(String[] args) {

        Client kate = new Client("Kate", "DeutscheBank", 2000);
        Person did = new Client("did", "did", 12312);
        kate.display();
        kate.name = "123123213";
        ((Client)kate).display();
        ((Client)kate).sum = 1222;
        Object sam = new Employee("Sam", "Oracle");
        ((Employee)sam).display();

    }
}
// класс человека
class Person {

    public String name;

    public String getName() { return name; }

    public Person(String name){

        this.name=name;
    }

    public void display(){

        System.out.printf("Person %s \n", name);
    }
}
// служащий некоторой компании
class Employee extends Person{

    private String company;

    public Employee(String name, String company) {

        super(name);
        this.company = company;
    }
    public String getCompany(){ return company; }

    public void display(){

        System.out.printf("Employee %s works in %s \n", super.getName(), company);
    }
}
// класс клиента банка
class Client extends Person{

    public int sum = 300; // Переменная для хранения суммы на счете
    private String bank;

    public Client(String name, String bank, int sum) {

        super(name);
        this.bank=bank;
        this.sum = sum;
    }

    public void display(){

        System.out.printf("Client %s has account in %s \n", super.getName(),  this.sum );

    }

    public String getBank(){ return bank; }
    public int getSum(){ return sum; }
}
