package qopp;

import jdk.nashorn.internal.parser.JSONParser;

public class LocalMAin {
    public static void main(String[] args) {


    //   we can't instantiated Abstract Class
    manager Bob = new manager();
    System.out.println(Bob.getName());
    Bob.setName("Bob");
    System.out.println(Bob.getName());
    Bob.displayInfo();
    Bob.setId("Bobbbb");
    Bob.displayInfo();
    Bob.Value("Bobbbb");
    System.out.println(Bob.getSalary());
    Bob.display();
    System.out.println();
    System.out.println();

    cashier Ania = new cashier("Ania", 1);
    Ania.displayInfo();
    Ania.Value("Ania");
    System.out.println(Ania.getSalary());
    System.out.println(Ania.getId());
    System.out.println();
    System.out.println();

    AbstractBankPeople Tom = new manager("Tom", 5);
    System.out.print("display : ");
    Tom.display();
    System.out.print("display : ");
    ((AbstractBankPeople)Tom).display();
    System.out.print("display : ");
    ((manager)Tom).display();
//        different result because of static method.
    System.out.println();
    System.out.println();

    AbstractBankPeople Jarry = new cashier();


    ((cashier)Jarry).setChild(3);
//   will don`t work     Jarry.setChild(3);
     Jarry.Value("jarryyyy");
     ((AbstractBankPeople)Jarry).display();
     System.out.println(((cashier)Jarry).getSalary());
//   will don`t work     System.out.println(Jarry.getSalary());

//        create object of class 4




//        MASS
     AbstractBankPeople[] BankPeople = { Bob, Ania, Tom, Jarry };
    for( AbstractBankPeople variable : BankPeople ){
        variable.displayInfo();
//      will don`t work because AbstractBankPeople haven't this method           variable.getSalary();
        variable.display();
        System.out.println();
        }
        Somebody some = new Somebody("some", 34);
    Interface[] InterfacePeople = { Bob, Ania, (Interface) Tom, (Interface)Jarry, some };

    for(Interface obs : InterfacePeople){
        System.out.println(obs.print());
        obs.prank();
        System.out.println();
    }

    manager mankind = new manager("mankind", 12 );
    AbstractBankPeople dniknam = new manager("dniknam", 13);
    mankind.SalaryValue("mankind");
    dniknam.SalaryValue("dniknam");
//    mankind and dniknam return manager.SalaryValue()




    dniknam.display();
    mankind.display();
//    mankind and dniknam return display() of hes tip.
        System.out.println();
        System.out.println();


    dniknam.display();
//    we can't Override method if super class's method is final
    mankind.display();
//    mankind and dniknam return display() of hes tip.
        System.out.println();
        System.out.println();


     System.out.println(AbstractBankPeople.HumanName);
//       will work only if experience be public or protected and not private static
     AbstractBankPeople.display();

     manager Ilon = new manager();
        System.out.println(Ilon.gettSTANTIC());;


    }
}