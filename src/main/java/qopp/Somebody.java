package qopp;


public class Somebody implements Interface {
    protected int variableInt;
    protected String variableString;

    Somebody(String variableString, int variableInt){
        this.variableString = variableString;
        this.variableInt = variableInt;
        System.out.println( " variableString " + this.variableString + " variableInt " + this.variableInt );
    }
    public String   getVariableString(){return this.variableString; }

    public int      getVariableInt(){return this.variableInt;}

    public void     setVariableString(String VariableString){this.variableString = VariableString; }

    public void     setVariableInt(int VariableInt){this.variableInt = VariableInt; }

    @Override
    public int print() {
        return  3;
    }

    @Override
    public void prank() {
        System.out.println("SomeBody");
    }
}
